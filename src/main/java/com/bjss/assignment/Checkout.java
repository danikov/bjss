package com.bjss.assignment;

import com.bjss.assignment.data.Basket;
import com.bjss.assignment.data.Item;
import com.bjss.assignment.data.Store;
import com.bjss.assignment.offers.Offer;
import com.bjss.assignment.util.CurrencyUtil;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.List;

public class Checkout { // TODO: this class is worth testing but as it orchestrates the whole process it starts to go beyond unit testing
    public static void checkout(Basket basket, Store store) {
        if (basket == null) {
            System.out.println("Invalid Basket");
            return;
        }

        if (!basket.getUnknownItems().isEmpty()) {
            System.out.println(String.format("Unknown items in the bagging area: %s",
                    Joiner.on(", ").join(basket.getUnknownItems())));
            return;
        }

        BigDecimal subtotal = CurrencyUtil.totalPrice(basket.getItems());
        System.out.println(String.format("Subtotal: %s", CurrencyUtil.getFormattedPrice(subtotal)));

        List<Item> remainingItems = basket.getItems();
        List<String> offerResults = Lists.newArrayList();
        BigDecimal discount = BigDecimal.ZERO;

        for (Offer offer : store.getOffers()) {
            List<Item> matchedItems = offer.extractTargets(remainingItems);
            String message = offer.getMessage(matchedItems);
            if (!Strings.isNullOrEmpty(message)) {
                offerResults.add(message);
                discount = discount.add(offer.getDiscount(matchedItems));
                remainingItems.removeAll(matchedItems);
            }
        }

        if (offerResults.isEmpty()) {
            System.out.println("(no offers available)");
        } else {
            System.out.println(Joiner.on('\n').join(offerResults));
        }

        BigDecimal total = subtotal.add(discount);
        System.out.println(String.format("Total: %s", CurrencyUtil.getFormattedPrice(total)));
    }
}
