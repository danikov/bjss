package com.bjss.assignment.data;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.Value;

import java.util.List;

/**
 * Responsible for parsing Items from an input String
 */
@Value
public class Basket {
    private static final String PREFIX = "PriceBasket";

    private List<Item> items;
    private List<String> unknownItems;

    /**
     * @param input non-null input String, should be prefixed with "PriceBasket "
     * @param store Store object against which items will be checked against stock
     * @return a Basket of stocked Items and unknown item Strings
     */
    public static Basket fromInput(String input, Store store) {
        if (store == null || input == null || !input.startsWith(PREFIX)) {
            return null;
        }

        List<Item> items = Lists.newArrayList();
        List<String> unknownItems = Lists.newArrayList();

        for (String itemName : Splitter.on(" ").split(input.substring(PREFIX.length()))) {
            if (!Strings.isNullOrEmpty(itemName)) {
                Item item = store.getStockByName(itemName);
                if (item != null) {
                    items.add(item);
                } else {
                    unknownItems.add(itemName);
                }

            }
        }

        return new Basket(items, unknownItems);
    }
}
