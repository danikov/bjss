package com.bjss.assignment.data;

import com.bjss.assignment.offers.Offer;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.Map;
import java.util.Set;

/**
 * A Store keeps track of registered Items that are in Stock and any current Offers
 */
public class Store {
    private Map<String, Item> stock = Maps.newHashMap();
    private Set<Offer> offers = Sets.newHashSet();

    public Store() {
    } // no-op

    public Store(Set<Item> items, Set<Offer> offers) {
        items.forEach(this::addToStock);
        offers.forEach(this::addOffer);
    }

    public void addToStock(Item item) {
        stock.put(item.getName(), item);
    }

    public Item getStockByName(String name) {
        return stock.get(name);
    }

    public void addOffer(Offer offer) {
        offers.add(offer);
    }

    public Set<Offer> getOffers() {
        return offers;
    }

    // TODO add ways to remove Items from Stock and cease Offers, simple remove methods
}
