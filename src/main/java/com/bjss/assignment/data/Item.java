package com.bjss.assignment.data;

import com.bjss.assignment.util.CurrencyUtil;
import lombok.Value;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Value
public class Item {
    String name;
    String unit;
    BigDecimal price;

    public Item(String name, String unit, BigDecimal price) {
        this.name = name;
        this.unit = unit;
        this.price = price.setScale(2, RoundingMode.HALF_UP);
    }

    public String toStringFull() {
        return String.format("%s – %s per %s", name, CurrencyUtil.getFormattedPrice(price), unit);
    }

    @Override
    public String toString() {
        return name;
    }
}
