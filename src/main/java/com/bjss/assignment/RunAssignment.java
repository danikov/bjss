package com.bjss.assignment;

import com.bjss.assignment.data.Basket;
import com.bjss.assignment.data.Item;
import com.bjss.assignment.data.Store;
import com.bjss.assignment.offers.ItemDiscount;
import com.bjss.assignment.offers.NAItemsFixedDiscountB;
import com.google.common.collect.Sets;

import java.math.BigDecimal;
import java.util.Scanner;

public class RunAssignment {
    private Item soup = new Item("Soup", "tin", BigDecimal.valueOf(0.65));
    private Item bread = new Item("Bread", "loaf", BigDecimal.valueOf(0.80));
    private Item milk = new Item("Milk", "bottle", BigDecimal.valueOf(1.30));
    private Item apples = new Item("Apples", "bag", BigDecimal.valueOf(1));

    ItemDiscount itemDiscountOffer = new ItemDiscount(apples, BigDecimal.valueOf(10));
    NAItemsFixedDiscountB naItemsFixedDiscountB = new NAItemsFixedDiscountB(
            soup, 2, bread, BigDecimal.valueOf(0.4));

    private Store store = new Store(
            Sets.newHashSet(apples, milk, bread, soup),
            Sets.newHashSet(itemDiscountOffer, naItemsFixedDiscountB)
    );

    public static void main(String[] args) {
        new RunAssignment().run();
    }

    public void run() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Input a basket:");
        String line = reader.nextLine();
        reader.close();

        Basket basket = Basket.fromInput(line, store);
        Checkout.checkout(basket, store);
    }
}
