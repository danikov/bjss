package com.bjss.assignment.offers;

import com.bjss.assignment.data.Item;
import com.bjss.assignment.util.CurrencyUtil;
import lombok.Value;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Offers a fixed percentage discount to all Items of the type discounted
 */
@Value
public class ItemDiscount implements Offer {
    private Item discountItem;
    private BigDecimal discountPercentage;
    private BigDecimal discount;

    public ItemDiscount(Item discountItem, BigDecimal discountPercentage) {
        this.discountItem = discountItem;
        this.discountPercentage = discountPercentage;
        this.discount = discountPercentage.setScale(2, RoundingMode.HALF_UP)
                .divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP);
    }

    public List<Item> extractTargets(Collection<Item> items) {
        return items.stream()
                .filter(item -> item.equals(discountItem))
                .collect(Collectors.toList());
    }

    public String getMessage(Collection<Item> items) {
        if (items.isEmpty()) {
            return "";
        }

        return String.format("%s %s off: %s", discountItem.getName(),
                NumberFormat.getPercentInstance().format(discount),
                CurrencyUtil.getFormattedPrice(getDiscount(items)));
    }

    public BigDecimal getDiscount(Collection<Item> items) {
        if (items.size() == 0) {
            return BigDecimal.ZERO;
        }

        BigDecimal total = CurrencyUtil.totalPrice(items.stream()
                .filter(i -> Objects.equals(i, discountItem))
                .collect(Collectors.toList()));

        return total.multiply(discount).multiply(BigDecimal.valueOf(-1));
    }
}
