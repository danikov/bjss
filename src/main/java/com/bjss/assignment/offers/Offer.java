package com.bjss.assignment.offers;

import com.bjss.assignment.data.Item;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * Provides an interface for applying Offers
 */
public interface Offer {
    /**
     * @param items the whole set of Items to examine
     * @return all Items that may become ineligible for other offers.
     * Should also be passed to $getMessage and $getDiscount.
     */
    List<Item> extractTargets(Collection<Item> items);

    /**
     * @param items extracted target Items
     * @return a user-friendly message describing the discount for said items
     */
    String getMessage(Collection<Item> items);

    /**
     * @param items extracted target Items
     * @return the sum total discount; will be negative if not 0
     */
    BigDecimal getDiscount(Collection<Item> items);
}
