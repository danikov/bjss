package com.bjss.assignment.offers;

import com.bjss.assignment.data.Item;
import com.bjss.assignment.util.CurrencyUtil;
import lombok.Value;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Offers a fixed discount for every 1 Item B (discount) per n Items A (qualifying)
 */
@Value
public class NAItemsFixedDiscountB implements Offer {
    private Item qualifyingItem;
    private Integer qualifierCount;
    private Item discountItem;
    private BigDecimal discount;

    /**
     * @param items the whole set of Items to examine
     * @return only the discounted items themselves; qualifying items are not discounted themselves
     */
    public List<Item> extractTargets(Collection<Item> items) {
        Collection<Item> qualifiers = items.stream()
                .filter(item -> item.equals(qualifyingItem))
                .collect(Collectors.toList());

        Integer discountable = (int) items.stream()
                .filter(item -> item.equals(discountItem))
                .count();

        Integer maxQualifyingDiscounts = qualifiers.size() / qualifierCount;
        Integer qualifyingDiscounts = discountable > maxQualifyingDiscounts ? maxQualifyingDiscounts : discountable;

        return IntStream.range(0, qualifyingDiscounts)
                .mapToObj(i -> discountItem)
                .collect(Collectors.toList());
    }

    public String getMessage(Collection<Item> items) {
        if (items.isEmpty()) {
            return "";
        }

        return String.format("Buy %d %s of %s and get a %s of %s for %s off: %s",
                qualifierCount, qualifyingItem.getUnit() + "s", qualifyingItem.getName(),
                discountItem.getUnit(), discountItem.getName(), CurrencyUtil.getFormattedPrice(discount),
                CurrencyUtil.getFormattedPrice(getDiscount(items)));
    }

    public BigDecimal getDiscount(Collection<Item> items) {
        if (items.size() == 0) {
            return BigDecimal.ZERO;
        }

        BigDecimal total = items.stream()
                .filter(Objects::nonNull)
                .filter(i -> Objects.equals(i, discountItem))
                .map(i -> discount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return total.multiply(BigDecimal.valueOf(-1));
    }
}
