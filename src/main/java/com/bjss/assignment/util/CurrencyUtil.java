package com.bjss.assignment.util;

import com.bjss.assignment.data.Item;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Locale;
import java.util.Objects;

public class CurrencyUtil {
    /**
     * @param price BigDecimal representing currency
     * @return a formatted String; for prices between £-1.00 and £1.00, it will instead use pence, e.g. -99p or 99p
     */
    public static String getFormattedPrice(BigDecimal price) {
        if (price.intValue() > -1 && price.intValue() < 1) {
            return NumberFormat.getIntegerInstance().format(price.remainder(BigDecimal.ONE).doubleValue() * 100) + "p";
        }
        return NumberFormat.getCurrencyInstance(Locale.UK).format(price);
    }

    /**
     * @return the sum total of the Item's prices
     */
    public static BigDecimal totalPrice(Collection<Item> items) {
        return items.stream()
                .filter(Objects::nonNull)
                .map(Item::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
