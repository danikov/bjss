package com.bjss.assignment.util;

import com.bjss.assignment.data.ItemTest;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CurrencyUtilTest {

    @Test
    public void getFormattedPrice() {
        assertEquals("£1.00", CurrencyUtil.getFormattedPrice(BigDecimal.valueOf(1)));
        assertEquals("99p", CurrencyUtil.getFormattedPrice(BigDecimal.valueOf(0.99)));
        assertEquals("1p", CurrencyUtil.getFormattedPrice(BigDecimal.valueOf(0.01)));
        assertEquals("0p", CurrencyUtil.getFormattedPrice(BigDecimal.valueOf(0)));
        assertEquals("-1p", CurrencyUtil.getFormattedPrice(BigDecimal.valueOf(-0.01)));
        assertEquals("-99p", CurrencyUtil.getFormattedPrice(BigDecimal.valueOf(-0.99)));
        assertEquals("-£1.00", CurrencyUtil.getFormattedPrice(BigDecimal.valueOf(-1)));
    }

    @Test
    public void totalPrice() {
        assertEquals(BigDecimal.valueOf(4.75), CurrencyUtil.totalPrice(
                Lists.newArrayList(ItemTest.apples, ItemTest.apples, ItemTest.milk, ItemTest.bread, ItemTest.soup)));
    }
}