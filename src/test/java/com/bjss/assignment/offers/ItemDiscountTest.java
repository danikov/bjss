package com.bjss.assignment.offers;

import com.bjss.assignment.data.Item;
import com.bjss.assignment.data.ItemTest;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ItemDiscountTest {
    public static ItemDiscount offer = new ItemDiscount(ItemTest.apples, BigDecimal.valueOf(10));

    @Test
    public void testExtractTargetsFromEmptyList() {
        List<Item> items = Collections.emptyList();

        assertEquals(items, offer.extractTargets(items));
    }

    @Test
    public void testExtractTargetsFromAbsentList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.soup, ItemTest.milk, ItemTest.milk, ItemTest.bread);
        List<Item> expectedItems = Collections.emptyList();

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testExtractTargetsFromList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.soup, ItemTest.milk, ItemTest.apples, ItemTest.bread);
        List<Item> expectedItems = Lists.newArrayList(ItemTest.apples);

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testExtractMultipleTargetsFromList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.apples, ItemTest.soup, ItemTest.milk, ItemTest.apples,
                ItemTest.bread, ItemTest.apples);
        List<Item> expectedItems = Lists.newArrayList(ItemTest.apples, ItemTest.apples, ItemTest.apples);

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testDiscountNoItems() {
        List<Item> items = Collections.emptyList();

        assertEquals(0, BigDecimal.ZERO.compareTo(offer.getDiscount(items)));
    }

    @Test
    public void testDiscountOneItems() {
        List<Item> items = Lists.newArrayList(ItemTest.apples);

        assertEquals(0, BigDecimal.valueOf(-0.1).compareTo(offer.getDiscount(items)));
    }

    @Test
    public void testDiscountManyItems() {
        List<Item> items = Lists.newArrayList(ItemTest.apples, ItemTest.apples, ItemTest.apples);

        assertEquals(0, BigDecimal.valueOf(-0.3).compareTo(offer.getDiscount(items)));
    }

    @Test
    public void testMessageNoItems() {
        List<Item> items = Collections.emptyList();

        assertEquals("", offer.getMessage(items));
    }

    @Test
    public void testMessageOneItems() {
        List<Item> items = Lists.newArrayList(ItemTest.apples);

        assertEquals("Apples 10% off: -10p", offer.getMessage(items));
    }

    @Test
    public void testMessageManyItems() {
        List<Item> items = Lists.newArrayList(ItemTest.apples, ItemTest.apples, ItemTest.apples);

        assertEquals("Apples 10% off: -30p", offer.getMessage(items));
    }
}