package com.bjss.assignment.offers;

import com.bjss.assignment.data.Item;
import com.bjss.assignment.data.ItemTest;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NAItemsFixedDiscountBTest {
    public static NAItemsFixedDiscountB offer = new NAItemsFixedDiscountB(
            ItemTest.soup, 2, ItemTest.bread, BigDecimal.valueOf(0.4));

    @Test
    public void testExtractTargetsFromEmptyList() {
        List<Item> items = Collections.emptyList();

        assertEquals(items, offer.extractTargets(items));
    }

    @Test
    public void testExtractTargetsFromNoAInList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.apples, ItemTest.milk, ItemTest.milk, ItemTest.bread);
        List<Item> expectedItems = Collections.emptyList();

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testExtractTargetsFromInsufficientAInList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.apples, ItemTest.soup, ItemTest.milk, ItemTest.bread);
        List<Item> expectedItems = Collections.emptyList();

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testExtractTargetsFromNoBInList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.soup, ItemTest.soup, ItemTest.milk, ItemTest.apples);
        List<Item> expectedItems = Lists.newArrayList();

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testExtractTargetsFromList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.soup, ItemTest.soup, ItemTest.apples, ItemTest.bread);
        List<Item> expectedItems = Lists.newArrayList(ItemTest.bread);

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testExtractMultipleTargetsFromList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.soup, ItemTest.soup, ItemTest.milk, ItemTest.apples,
                ItemTest.bread, ItemTest.apples, ItemTest.soup, ItemTest.bread, ItemTest.soup);
        List<Item> expectedItems = Lists.newArrayList(ItemTest.bread, ItemTest.bread);

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testExtractMultiplePartialTargetsFromList() {
        List<Item> sourceItems = Lists.newArrayList(ItemTest.soup, ItemTest.soup, ItemTest.milk, ItemTest.apples,
                ItemTest.bread, ItemTest.apples, ItemTest.soup, ItemTest.bread, ItemTest.soup, ItemTest.soup, ItemTest.bread);
        List<Item> expectedItems = Lists.newArrayList(ItemTest.bread, ItemTest.bread);

        assertEquals(expectedItems, offer.extractTargets(sourceItems));
    }

    @Test
    public void testDiscountNoItems() {
        List<Item> items = Collections.emptyList();

        assertEquals(0, BigDecimal.ZERO.compareTo(offer.getDiscount(items)));
    }

    @Test
    public void testDiscountOneItems() {
        List<Item> items = Lists.newArrayList(ItemTest.soup, ItemTest.soup, ItemTest.bread);

        assertEquals(0, BigDecimal.valueOf(-0.4).compareTo(offer.getDiscount(items)));
    }

    @Test
    public void testDiscountManyItems() {
        List<Item> items = Lists.newArrayList(
                ItemTest.soup, ItemTest.soup, ItemTest.soup,
                ItemTest.soup, ItemTest.soup, ItemTest.soup,
                ItemTest.bread, ItemTest.bread, ItemTest.bread);

        assertEquals(0, BigDecimal.valueOf(-1.2).compareTo(offer.getDiscount(items)));
    }

    @Test
    public void testMessageNoItems() {
        List<Item> items = Collections.emptyList();

        assertEquals("", offer.getMessage(items));
    }

    @Test
    public void testMessageOneItems() {
        List<Item> items = Lists.newArrayList(ItemTest.soup, ItemTest.soup, ItemTest.bread);

        assertEquals("Buy 2 tins of Soup and get a loaf of Bread for 40p off: -40p", offer.getMessage(items));
    }

    @Test
    public void testMessageManyItems() {
        List<Item> items = Lists.newArrayList(
                ItemTest.soup, ItemTest.soup, ItemTest.soup,
                ItemTest.soup, ItemTest.soup, ItemTest.soup,
                ItemTest.bread, ItemTest.bread, ItemTest.bread);

        assertEquals("Buy 2 tins of Soup and get a loaf of Bread for 40p off: -£1.20", offer.getMessage(items));
    }
}