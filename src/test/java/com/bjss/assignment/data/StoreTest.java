package com.bjss.assignment.data;

import com.bjss.assignment.offers.ItemDiscountTest;
import com.bjss.assignment.offers.NAItemsFixedDiscountBTest;
import com.google.common.collect.Sets;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class StoreTest {
    public static Store defaultStore() {
        return new Store(
                Sets.newHashSet(ItemTest.apples, ItemTest.milk, ItemTest.bread, ItemTest.soup),
                Sets.newHashSet(ItemDiscountTest.offer, NAItemsFixedDiscountBTest.offer)
        );
    }

    @Test
    public void testGetUnknownItem() {
        Store store = new Store();

        assertNull(store.getStockByName("not an item"));
    }

    @Test
    public void testAddAndGetItem() {
        Store store = new Store();

        store.addToStock(ItemTest.milk);

        assertEquals(ItemTest.milk, store.getStockByName("Milk"));
    }

}