package com.bjss.assignment.data;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class ItemTest {

    public static Item soup = new Item("Soup", "tin", BigDecimal.valueOf(0.65));
    public static Item bread = new Item("Bread", "loaf", BigDecimal.valueOf(0.80));
    public static Item milk = new Item("Milk", "bottle", BigDecimal.valueOf(1.30));
    public static Item apples = new Item("Apples", "bag", BigDecimal.valueOf(1));

    @Test
    public void testCreateItem() {
        assertEquals("Apples", apples.getName());
        assertEquals("bag", apples.getUnit());
        assertEquals("Apples", apples.toString());
        assertEquals("Apples – £1.00 per bag", apples.toStringFull());

        assertEquals("Soup – 65p per tin", soup.toStringFull());
        assertEquals("Bread – 80p per loaf", bread.toStringFull());
        assertEquals("Milk – £1.30 per bottle", milk.toStringFull());
    }
}