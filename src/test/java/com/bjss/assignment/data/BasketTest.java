package com.bjss.assignment.data;

import com.google.common.collect.Lists;
import org.junit.Test;

import static org.junit.Assert.*;

public class BasketTest {
    @Test
    public void testInvalidPrefix() {
        Store store = StoreTest.defaultStore();

        Basket basket = Basket.fromInput("badprefix apples soup ", store);

        assertNull(basket);
    }

    @Test
    public void testEmptyBasket() {
        Store store = StoreTest.defaultStore();

        Basket basket = Basket.fromInput("PriceBasket ", store);

        assertNotNull(basket);
        assertEquals(0, basket.getItems().size());
        assertEquals(0, basket.getUnknownItems().size());
    }

    @Test
    public void testBasketWithUnknownObject() {
        Store store = StoreTest.defaultStore();

        Basket basket = Basket.fromInput("PriceBasket foo bar", store);

        assertNotNull(basket);
        assertEquals(0, basket.getItems().size());
        assertEquals(Lists.newArrayList("foo", "bar"), basket.getUnknownItems());
    }

    @Test
    public void testBasketWithAnItem() {
        Store store = StoreTest.defaultStore();

        Basket basket = Basket.fromInput("PriceBasket Soup", store);

        assertNotNull(basket);
        assertEquals(Lists.newArrayList(ItemTest.soup), basket.getItems());
        assertEquals(0, basket.getUnknownItems().size());
    }

    @Test
    public void testBasketWithManyItems() {
        Store store = StoreTest.defaultStore();

        Basket basket = Basket.fromInput("PriceBasket Soup Soup Soup Soup Soup", store);

        assertNotNull(basket);
        assertEquals(
                Lists.newArrayList(ItemTest.soup, ItemTest.soup, ItemTest.soup, ItemTest.soup, ItemTest.soup),
                basket.getItems());
        assertEquals(0, basket.getUnknownItems().size());
    }

    @Test
    public void testBasketWithMixedItems() {
        Store store = StoreTest.defaultStore();

        Basket basket = Basket.fromInput("PriceBasket Soup Soup Soup Soup Soup", store);

        assertNotNull(basket);
        assertEquals(
                Lists.newArrayList(ItemTest.soup, ItemTest.soup, ItemTest.soup, ItemTest.soup, ItemTest.soup),
                basket.getItems());
        assertEquals(0, basket.getUnknownItems().size());
    }
}